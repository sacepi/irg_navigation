#!/usr/bin/env python
import rospy
import tf
import math
import random
from std_msgs.msg import String
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Pose, Quaternion	
from geometry_msgs.msg import Twist
from shapely.geometry import Point,Polygon,LineString,box

class Trajectory:
#This class generates a parametrization of the trajectory, which will be followed in time
	def __init__(self):
		self.waypoints = list()
		self.vel = 3 #Indicates the mean velocity (in m/s) which the robot is suposed to have to follow the trajectory
		self.t_init = 0; #do not change this. The real initial time will be assigned in the initTraj method
		self.discreteDistance = 0.01 #This variable gives the discrete distance between two waypoints 
		self.traj = list()
	def eval(self,time):
	#This method returns the position of a trajectory at a given instant of time
		if time < 0:
			rospy.loginfo("Negative time!")
			return self.traj[0],[0,0]		
		normalizedTime = (time-self.t_init)*self.vel/self.discreteDistance		
		index = int(math.floor(normalizedTime))
		if index < len(self.traj)-1:
			error = normalizedTime - index
			pos = [0 for i in range(len(self.traj[index]))]			
			for subIndex in range(len(self.traj[index])):			
				pos[subIndex] = self.traj[index][subIndex] + error*(self.traj[index+1][subIndex] - self.traj[index][subIndex])
			vel = [0 for i in range(len(self.traj[index]))]	
			for subIndex in range(len(self.traj[index])):			
				vel[subIndex] = self.vel*(self.traj[index+1][subIndex] - self.traj[index][subIndex])/self.discreteDistance
			return pos,vel
		else:
			return self.traj[len(self.traj)-1],[0,0]
	def initTraj(self,waypoints):
	#This method initializate the trajectory given the waypoints
		self.waypoints = waypoints
		self.traj = self.generateTrajectory()
		self.t_init = rospy.get_time()
	def getTraj(self):
		return self.eval(rospy.get_time())
	def generateTrajectory(self):
		traj = list()		
		dist = self.discreteDistance
		pos = [0 for i in range(len(self.waypoints[0]))]			
		wayIndex = 0
		acc = 0
		traj.append([self.waypoints[0][i] for i in range(len(self.waypoints[0]))])
		while wayIndex < len(self.waypoints)-1:
			totalDistance = math.sqrt((self.waypoints[wayIndex][0]-self.waypoints[wayIndex+1][0])**2 + (self.waypoints[wayIndex][1]-self.waypoints[wayIndex+1][1])**2)
			currentDistance = 0
			while (totalDistance - currentDistance + acc) > dist:
				currentDistance = currentDistance + dist - acc
				acc = 0
				for subIndex in range(len(self.waypoints[wayIndex])):
					pos[subIndex] = self.waypoints[wayIndex][subIndex] + currentDistance/totalDistance*(self.waypoints[wayIndex+1][subIndex]-self.waypoints[wayIndex][subIndex])
				traj.append([pos[i] for i in range(len(pos))])
			acc = acc + totalDistance - currentDistance
			wayIndex = wayIndex +1
		traj.append(self.waypoints[len(self.waypoints)-1])
		return traj
				
class RRT:
#This class handles all the calculations related to the RRT algorithm for feasible path planning
	def __init__(self):
	#Constructor. Sets the initial values
		
		self.traj = 0; #Just initializing this variable. It is not necessary to change it.
		
		#Sets the limits of the area in which the robot is allowed to move, both in X and Y directions 
		self.xLimits = [-0.8,0.8];
		self.yLimits = [-1.2,1.2];
		
		#Set the maximum distance between two RRT nodes. If a smoother path is desired, use a small value.
		#The smaller this value is, the slower will be the RRT algorithm
		self.steerConst = 0.01;
		
		#This variable is used by the collision checking. It indicates the safe distance between the center of the robot and the corner of the object.		
		self.radius = 0.5;

		#This variable is a list of Polygons, each one describing a polygonal obstacle.
		self.obstacles = [Polygon([(0.3,0.15),(0.1,0.15),(0.1,0.25),(0.3,0.25)])]

		#This variable defines the max number of iterations before the RRT consider the path unfeasible.		
		self.maxIter = 100000;
	
	def rrt(self,currentState,goal):
	#This function actually implements the RRT algorithm		
		
		parentList = list(); #This list keeps track of the parent of each node, to generate the path after the RRT finishes.
		nodes = list()  #This is a list of nodes
		nodes.append(currentState); #Start the list with the current robot state
		
		finished = False
		newPoint = [0,0]
		auxPoint = [0,0]
		
		parentList.append(-1) #This indicates that the currentState is the initial state of the robot (i.e. the first node of the tree, which has no parent)
		
		numIter = 0;	

		while not finished and numIter<self.maxIter:
			numIter = numIter+1		
			#Sampling a random number to indicate whether the RRT algorithm will sample the goal or a random position		
			sampleIndicator = random.uniform(0,1)
			
			if sampleIndicator < 0.95: #Sampling a random position if 95% of probability
				newPoint[0] = random.uniform(self.xLimits[0],self.xLimits[1])
				newPoint[1] = random.uniform(self.yLimits[0],self.yLimits[1])
			else:
				newPoint = [goal[i] for i in range(len(goal))] #Sampling the goal, to bias the tree towards the goal
			
			closestIndex = self.findClosestNode(newPoint,nodes) #Finding the index of the closest node to the sampled node
			
			if self.distance(newPoint,nodes[closestIndex]) > self.steerConst:
				#Performing the steering				
				auxPoint[0] = nodes[closestIndex][0]+self.steerConst/self.distance(newPoint,nodes[closestIndex])*(newPoint[0]-nodes[closestIndex][0])
				auxPoint[1] = nodes[closestIndex][1]+self.steerConst/self.distance(newPoint,nodes[closestIndex])*(newPoint[1]-nodes[closestIndex][1])
				newPoint = auxPoint			
			
			if self.isValid(newPoint):#Checking if the sampled point does not collide with any obstacle
				parentList.append(closestIndex) #If the new point is consistent, add to the tree
				nodes.append([newPoint[i] for i in range(len(newPoint))])
				
				if self.distance(newPoint,goal) < 1e-3: #Checks if the algorithm has finished
					finished = True
		if not finished: #If the RRT does not finish before the maximum number of iterations (probably infeasible trajectory), return None
			print 'RRT could not find a feasible plan'
			return None
		
		#Generating the path from the RRT tree
		waypoints = list()
		waypoints.append(nodes[len(nodes)-1])		
		currentIndex = parentList[len(parentList)-1]		
		while(currentIndex >= 0):
			waypoints.insert(0,nodes[currentIndex])
			currentIndex = parentList[currentIndex]
		
		print 'RRT successfully found a plan!'		
		print waypoints		
		return waypoints

	def findClosestNode(self,currentNode,nodes):
	#This function finds the closest node to a sampled point, given the nodes that already are in the tree.
		minDist = self.distance(currentNode,nodes[0])
		minIndex = 0;
		for index in range(1,len(nodes)):
			if self.distance(currentNode,nodes[index]) < minDist:
				minDist = self.distance(currentNode,nodes[index])
				minIndex = index
		return minIndex	
	
	def distance(self,node1,node2):
	#This function calculates the distance between two points in the space
		return math.sqrt((node1[0]-node2[0])**2+(node1[1]-node2[1])**2)

	def isValid(self,pointSampled):		
	#This function performs the check of feasibility of a given point.
		valid = True
		point = Point(pointSampled)
		circle = point.buffer(self.radius,resolution=3)
		for index in range(len(self.obstacles)):
			if circle.intersects(self.obstacles[index]) or self.obstacles[index].contains(point):
				valid = False
		return valid	
					

def IRGNavigation():
	#Setting up parameters
	isSimulation = True	
	K_p = 3 #Proportional gain od the controller
	maxFollowingError = 0.1 #Distance between the original plan and the actual robot position from which the robot will replan its path
	
	#Initializing variables	
	rrtHandler = RRT()
	traj = Trajectory()
	twist = Twist()
	hasPlan = False	
	
	#Initializing the navigation node
	rospy.init_node('IRG_Navigation')    	
	

	#Setting the frames for the tf transform which will read the position and orientation of the robot
	if isSimulation:	
		base_frame_id = rospy.get_param("~base_frame_id", "base_link")
    		odom_frame_id = rospy.get_param("~odom_frame_id", "odom")
	else:
		#These settings depend on which vicon frames you are using
		base_frame_id = rospy.get_param("~base_frame_id", "vicon/irgKooka2/irgKooka2")
    		odom_frame_id = rospy.get_param("~odom_frame_id", "world")		    	
	
	#Initializing the tf listener and the velocity publisher.
	listener = tf.TransformListener()
    	pub = rospy.Publisher('cmd_vel', Twist)
    	
		
	rospy.loginfo("Reading position from \"%s\" to \"%s\"", odom_frame_id, base_frame_id)
	
	
	while not rospy.is_shutdown():
		#Listening to the transform that gives the position of the robot
		try:
			(trans,rot) = listener.lookupTransform(base_frame_id, odom_frame_id, rospy.Time(0))
		except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
			#rospy.loginfo("Could not find required transform")		
			continue
		
		#Replaning the motion in case it is not what it was expected to be	
		if not hasPlan:
			waypoints = rrtHandler.rrt([trans[0],trans[1]],[0.3,-0.83])		
			if waypoints != None:			
				traj.initTraj(waypoints)	
				hasPlan = True	
		
		[pos,vel] = traj.getTraj()			
		
		#Calculating the error of the trajectory following
		xError = trans[0]-pos[0]
		yError = trans[1]-pos[1]

		#Checking if the robot could keep the plan. If it gets to far from the original plan, replan.		
		if xError**2+yError**2 > maxFollowingError**2:
			hasPlan = False
			print "The robot was too far from its original plan. Need to replan."
		
		#Estimating the robot angle
		psi = math.atan2(2*(rot[0]*rot[3]+rot[1]*rot[2]),1-2*(rot[2]**2+rot[3]**2))
		print psi

		if hasPlan:
			#Linear controller for following the trajectory
			#twist.linear.x = (vel[0]+K_p*(xError))*math.cos(psi)+(vel[1]+K_p*(yError))*math.sin(psi)
			#twist.linear.y = -1*(vel[0]+K_p*(xError))*math.sin(psi)+(vel[1]+K_p*(yError))*math.cos(psi)
			twist.linear.x = vel[0]+K_p*(xError)
			twist.linear.y = vel[1]+K_p*(yError)			
			twist.linear.z = 0
		
			#Setting control commands to zero in case they are very small
			#This prevents the robot of doing small oscilations
			if (twist.linear.x**2 + twist.linear.y**2) < 1e-3:			
				twist.linear.x = 0	
				twist.linear.y = 0
		
			twist.angular.x = 0 
			twist.angular.y = 0
			twist.angular.z = (psi-1)
		else:
			twist.linear.x = 0
			twist.linear.y = 0
			twist.linear.z = 0
			twist.angular.x = 0 
			twist.angular.y = 0
			twist.angular.z = 0
		
		#Publishing calculated velocity		
		pub.publish(twist)

if __name__ == '__main__':
	try:
	        IRGNavigation()
	except rospy.ROSInterruptException:
	        pass
